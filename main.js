var newList = [];

function deleteLine(row){
	var d = row.parentNode.parentNode.rowIndex;
	document.getElementById('myTable').deleteRow(d);
}

// function deleteTheLine(o){
    // var p = o.parentNode.parentNode;
    // p.parentNode.removeChild(p);
// }

function getMyTDTag(ref){
	var theTDRef = ref;
	var found = false;
	while ((theTDRef !== null) && (! found)) {
		if (theTDRef.nodeType === 3 ){
			theTDRef = theTDRef.parentNode;
		} else if (theTDRef.tagName === "TD") {
			found = true;
		} else {
			theTDRef = theTDRef.parentNode;
		}
	}
	return theTDRef;
}

function changeBGToRed(ref){
	var row = getMyTDTag(ref);
	// row.parentNode.className = "backgroundRed";
	row.parentNode.style.backgroundColor="red";
}

function toClassInit(ref) {
	var row = getMyTDTag(ref);
	// row.parentNode.className = "line";
	row.parentNode.style.backgroundColor="transparent";
}

function changeBackground(ref, color) {
	var row = getMyTDTag(ref);
	row.parentNode.style.backgroundColor=color;
}

function refresh() {
	$.ajax({
		url:"/tp2/ajaxItemCategory.php",
		data: {
			code: "pandemonium",
			action: "refresh",
		},
		method: "POST",
		success: function(result) {
			removeLines();
			var obj = result.data;
			for (var i=0; i<obj.length; i++){
				addNewLine(obj[i].id,obj[i].author,obj[i].title,obj[i].body);
			}
			console.log("ok");
		},	
		error: function (resultat, statut, erreur){
			console.log("error");
		}
	});
	newList = [];
}


// var list;
// var new_list;

// function deleteIdList(){
	// list = [];
	// list.push(document.getElementById('deleteId').value);
	// var num;
	// num = document.getElementById('deleteId').value;
	// if (num>0 && num<7){
	// list.push(num);
	// } else {
	// alert("Number of id is wrong!");
	// }
	// new_list=list.join(",");
	// var list=[];
	// var obj=document.getElementById("deleteId");
	// for(var i=0;i<6;i++){
		// if(obj.value>0){
			 // list.push(obj.value);
		// }
	// }
	// alert(list);
// }


function deleteItem() {
	var num = document.getElementById("deleteId").value;
	if (num == ""){
		num = 0;
	}
	if (! newList.includes(num)){
		newList.push(num);
	}
	$.ajax({
		url:"/tp2/ajaxItemCategory.php",
	
		data: {
			code: "pandemonium",
			action: "delete",
			deletedList: newList,
		},
		method: "POST",
		success: function(result) {		
			removeLines();
			var obj = result.data;
			for (var i=0; i<obj.length; i++){
				addNewLine(obj[i].id,obj[i].author,obj[i].title,obj[i].body);
			}	
			console.log(result);
			console.log("ok");
			
		},
		error: function(resultat, statut, erreur){
			console.log("error");
			// alert(newList);
		}
	});
	
	// var num = document.getElementById("deleteId").value;
    // if (num == "") {
        // num = 0;
    // }
    // if (! newList.includes(num)){
        // newList.push(num);
    // }
	// console.log(newList);
    // $.ajax({
        // url:"/tp2/ajaxItemCategory.php",
        // data: {
            // code:"pandemonium",
            // action:"delete",
            // deletedList: newList
        // },
        // method: "POST",
        // success: function(result){
            // removeLines();
            // var obj = result.data;
            // for(var i=0; i<obj.length;i++){
                // addNewLine(obj[i].id, obj[i].title, obj[i].author, obj[i].body);
            // }
            // removedidList = [];
            // console.log(result.deletedList);
        // },
        // error: function(result, statut, erreur) {
            // console.log("error")
        // }
    // });
	
}




// DOM methode 
function removeLines() {
	var listeTR = document.getElementsByTagName("TR");
	var toBeRemoved = new Array();
	for (var i=0; i < listeTR.length; i++) {
		var ref = listeTR[i];
		if (ref.className === "line") {
			toBeRemoved.push(ref);
		}
	}
	while (toBeRemoved.length > 0) {
		ref = toBeRemoved.pop();
		ref.parentNode.removeChild(ref);
	}
}

 // JQuery methode
 // function removeLines() {
	 // $(".line").remove();
 // }
 
function addNewLine(id, author, title, subject) {
	 var string="<tr class=\"line\">";
	 string += "<td>"+id+"</td>";
	 string += "<td>"+title+"</td>";
	 string += "<td>"+author+"</td>";
	 string += "<td>"+subject+"</td>";
	 string += '<td><button type="button" class="btn" onclick="deleteLine(this)" onmouseover="changeBGToRed(this)" onmouseleave="toClassInit(this)"><img src="trash.png"></button></td>';
	 string += "</tr>";
	 
	 $('#myTable tr:last').after(string);
 }
 
function addLine(id, author, title, subject) {
	 var string="<tr class=\"line\">";
	 string += "<td>"+id+"</td>";
	 string += "<td>"+title+"</td>";
	 string += "<td>"+author+"</td>";
	 string += "<td>"+subject+"</td>";
	 string += '<td class="backgroundInit"><input type="button" class="btn" onclick="deleteLine(this)" onmouseover="changeBGToRed(this)" onmouseleave="toClassInit(this)"><img src="trash.png"></button></td>';
	 string += "</tr>";
	 
	 $('#myTable tr:last').after(string);
 }
 
function add() {
	$.ajax({
		url:"/tp1/ajaxItemCategory.php",
		data: {
			code: "pandemonium"
		},
		method: "POST",
		success: function(result) {
			var obj = result.data;
			var i = Math.floor(Math.random() * 6) + 1 ;
			document.getElementById("title").value = obj[i].title;
			document.getElementById("author").value = obj[i].author;
			document.getElementById("body").value = obj[i].body;
			
			// addLine(obj[i].id,obj[i].author,obj[i].title,obj[i].body);
			
			console.log("ok");
		},	
		error: function (resultat, statut, erreur){
			console.log("error");
		}
	});
}

